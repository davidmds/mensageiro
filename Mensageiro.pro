#-------------------------------------------------
#
# Project created by QtCreator 2017-06-01T14:58:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Mensageiro
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS+=-lWs2_32
SOURCES += main.cpp\
        mensageiromain.cpp \
    dadosmensageiro.cpp \
    modelconversas.cpp \
    modelmensagens.cpp \
    logindialog.cpp \
    winsocket/winsocket.cpp \
    mensageiro/minimess.cpp \
    conversadialog.cpp

HEADERS  += mensageiromain.h \
    dadosmensageiro.h \
    modelconversas.h \
    modelmensagens.h \
    logindialog.h \
    winsocket/winsocket.h \
    socketmensageiro.h \
    mensageiro/minimess.h \
    config.h \
    conversadialog.h

FORMS    += mensageiromain.ui \
    logindialog.ui \
    conversadialog.ui
