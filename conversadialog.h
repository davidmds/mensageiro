#ifndef CONVERSADIALOG_H
#define CONVERSADIALOG_H

#include <QDialog>
#include <string>

using namespace std;

namespace Ui {
class ConversaDialog;
}

class ConversaDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConversaDialog(QWidget *parent = 0);
    ~ConversaDialog();
    void open();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ConversaDialog *ui;

signals:
    void novaConversa(const string &usuario);
};

#endif // CONVERSADIALOG_H
