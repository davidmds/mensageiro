#include <QMessageBox>
#include <iostream>
#include "config.h"
#include "mensageiromain.h"
#include "ui_mensageiromain.h"
#include "modelconversas.h"
#include "modelmensagens.h"
#include "dadosmensageiro.h"
#include "socketmensageiro.h"
#include "mensageiro/minimess.h"

using namespace std;

DWORD WINAPI le_msg(LPVOID lpParameter){
  int32_t tempInt, id;
  string usuario, texto;
  Mensagem M;
  int idConversa;

  while (!fim) {
    // Lê o comando
    if(!read_int(s, tempInt)) fim = true;
    if (!fim){
      switch(tempInt) {
        case CMD_NEW_USER:
          // Ignorar. Erro do servidor
          break;
        case CMD_LOGIN_USER:
          // Ignorar. Erro do servidor
          break;
        case CMD_LOGIN_OK:
          // Ignorar, pois não veio após um pedido de criação de usuário ou conexão de usuário
          break;
        case CMD_LOGIN_INVALIDO:
          // Ignorar, pois não veio após um pedido de criação de usuário ou conexão de usuário
          break;
        case CMD_NOVA_MSG:
          if(!read_int(s, id)){
            fim = true;
            break;
          }
          if(!read_string(s, usuario)){
            fim = true;
            break;
          }
          if(!read_string(s, texto)){
            fim = true;
            break;
          }

          idConversa = -1;
          for(unsigned i = 0; i < DM.size(); i++){
              if(DM[i].getUsuario() == usuario){
                  idConversa = i;
                  break;
              }
          }
          // Cria conversa se não existir
          if(idConversa == -1){
              idConversa = DM.size();
              DM.insertConversa(usuario);
          }
          M.setUsuario(usuario);
          M.setId(id);
          M.setTexto(texto);
          if(DM[DM.getIdConversa()].getUsuario() == usuario){
            M.setStatus(MSG_LIDA);
            write_int(s, CMD_MSG_LIDA1);
            write_int(s, id);
            write_string(s, usuario);

          } else
            M.setStatus(MSG_ENTREGUE);

          DM[idConversa].insertMessage(M);
          break;
        case CMD_MSG_RECEBIDA:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && DM[i][j].getStatus() == MSG_ENVIADA){
                      DM[i][j].setStatus(MSG_RECEBIDA);
                      break;
                  }
              }
          }
          break;
        case CMD_MSG_ENTREGUE:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && (DM[i][j].getStatus() == MSG_ENVIADA || DM[i][j].getStatus() == MSG_RECEBIDA)){
                      DM[i][j].setStatus(MSG_ENTREGUE);
                      break;
                  }
              }
          }
          break;
        case CMD_MSG_LIDA1:
          // Ignorar. Erro do servidor
          break;
        case CMD_MSG_LIDA2:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && (DM[i][j].getStatus() == MSG_ENVIADA || DM[i][j].getStatus() == MSG_RECEBIDA || DM[i][j].getStatus() == MSG_ENTREGUE)){
                      DM[i][j].setStatus(MSG_LIDA);
                      break;
                  }
              }
          }
          break;
        case CMD_ID_INVALIDA:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && DM[i][j].getStatus() == MSG_ENVIADA){
                      DM[i][j].setStatus(MSG_INVALIDA);
                      break;
                  }
              }
          }
          break;
        case CMD_USER_INVALIDO:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && DM[i][j].getStatus() == MSG_ENVIADA){
                      DM[i][j].setStatus(MSG_INVALIDA);
                      break;
                  }
              }
          }
          break;
        case CMD_MSG_INVALIDA:
          read_int(s, id);
          for(unsigned i = 0; i < DM.size(); i++){
              for(unsigned j = 0; j < DM[i].size(); j++){
                  if(DM[i][j].getId() == id && DM[i][j].getUsuario() == DM.getMeuUsuario() && DM[i][j].getStatus() == MSG_ENVIADA){
                      DM[i][j].setStatus(MSG_INVALIDA);
                      break;
                  }
              }
          }
          break;
        case CMD_LOGOUT_USER:
          // Ignorar. Erro do servidor
          break;
        default:
          // Ignorar. Erro do servidor
          break;
      }
    }
  }
  return 0;
}

MensageiroMain::MensageiroMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MensageiroMain)
{
    ui->setupUi(this);

    // A lista da esquerda (conversas)
    modelConversas = new ModelConversas(this);
    ui->tableViewConversas->setModel( modelConversas );
    ui->tableViewConversas->horizontalHeader()->setSectionResizeMode(0,QHeaderView::ResizeToContents);
    ui->tableViewConversas->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->tableViewConversas->horizontalHeader()->setSectionsClickable(false);
    ui->tableViewConversas->setShowGrid(false);
    ui->tableViewConversas->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableViewConversas->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableViewConversas->setTabKeyNavigation(false);

    // A lista da direita (mensagens)
    modelMensagens = new ModelMensagens(this);
    ui->tableViewMensagens->setModel( modelMensagens );
    ui->tableViewMensagens->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->tableViewMensagens->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->tableViewMensagens->horizontalHeader()->setSectionsClickable(false);
    ui->tableViewMensagens->setShowGrid(true);
    ui->tableViewMensagens->setSelectionMode(QAbstractItemView::NoSelection);
    ui->tableViewMensagens->setTabKeyNavigation(false);

    // A barra de status
    statusBar()->insertWidget(0,new QLabel("USUÁRIO: "));
    if (DM.getMeuUsuario() == "")
    {
        nomeUsuario = new QLabel("---");
    }
    else
    {
        nomeUsuario = new QLabel(DM.getMeuUsuario().c_str());
    }
    statusBar()->insertWidget(1,nomeUsuario);

    statusBar()->insertWidget(2,new QLabel("   CONVERSA: "));
    if (DM.getIdConversa() < 0)
    {
        nomeConversa = new QLabel("---");
    }
    else
    {
        nomeConversa = new QLabel(DM[DM.getIdConversa()].getUsuario().c_str());
    }
    statusBar()->insertWidget(3,nomeConversa);

    // A caixa de dialogo de login
    loginDialog = new LoginDialog(this);
    conversaDialog = new ConversaDialog(this);

    // As conexoes
    connect(this, SIGNAL (conversasModificada()),
            modelConversas, SLOT (slotAtualizaConversas()));
    connect(this, SIGNAL (numMsgConversaModificado(unsigned)),
            modelConversas, SLOT (slotAtualizaNumMsgConversa(unsigned)));
    connect(this, SIGNAL (mensagensModificada()),
            modelMensagens, SLOT (slotAtualizaMensagens()));
    connect(this, SIGNAL (statusModificada()),
            this, SLOT (slotAtualizaBarraStatus()));

    connect(loginDialog, SIGNAL (aceitaUsuario(const string, const string, bool)),
            this, SLOT (slotAceitaUsuario(const string, const string, bool)));

    connect(conversaDialog, SIGNAL (novaConversa(const string)),
            this, SLOT (slotNovaConversa(const string)));
}

MensageiroMain::~MensageiroMain()
{
    delete ui;
}

void MensageiroMain::slotAtualizaBarraStatus()
{
    if (DM.getMeuUsuario() == "")
    {
        nomeUsuario->setText("---");
    }
    else
    {
        nomeUsuario->setText(DM.getMeuUsuario().c_str());
    }

    if (DM.getIdConversa() < 0)
    {
        nomeConversa->setText("---");
    }
    else
    {
        nomeConversa->setText(DM[DM.getIdConversa()].getUsuario().c_str());
    }
}

void MensageiroMain::slotAceitaUsuario(const string &login,
                                       const string &senha,
                                       bool novoUsuario )
{
    int32_t tempInt;

    if (DM.getMeuUsuario().size())
    {
        QMessageBox::warning(this, "Login", "Desconecte-se antes de tentar logar novamente!");
        return;
    }

    bool usuarioOK = (login.size()>=TAM_MIN_NOME_USUARIO && login.size()<=TAM_MAX_NOME_USUARIO);

    if (usuarioOK)
    {
        usuarioOK = (senha.size()>=TAM_MIN_SENHA && senha.size()<=TAM_MAX_SENHA);
    }

    if (usuarioOK)
    {
        // Testa se eh possivel cadastrar um novo usuario com esses login e senha
        if(!s.connected()){
          // Conecta com o servidor
          if (s.connect(IP_SERVIDOR, PORTA_TESTE) != SOCKET_OK){
            QMessageBox::warning(this, "Login", "Problema na conexao ao servidor!");
            exit(1);
          }

          // Cria a thread que escreve as mensagens recebidas
          tHandle = CreateThread(NULL, 0, le_msg, NULL , 0, NULL);
          if (tHandle == NULL){
            cerr << "Problema na criacao da thread: " << GetLastError() << endl;
            exit(1);
          }
        }
        // Envia o comando
        if (novoUsuario)
            write_int(s, CMD_NEW_USER);
        else
            write_int(s, CMD_LOGIN_USER);
        // Envia os parametros do comando
        write_string(s, login);
        write_string(s, senha);
        read_int(s, tempInt);

        // Verifica o comando retornado
        if(tempInt != CMD_LOGIN_OK) usuarioOK = false;
    }

    if (!usuarioOK)
    {
        QMessageBox::warning(this, "Login", "Usuário e/ou senha incorretos...");
        return;
    }

    // Fixa o novo nome do usuário e limpa todas as conversas
    DM.setMeuUsuario(login);

    emit conversasModificada();
    emit mensagensModificada();
    emit statusModificada();

    QMessageBox::information(this, "Login", "Usuário conectado.");
}

void MensageiroMain::slotNovaConversa(const string &usuario)
{
    DM.insertConversa(usuario);
    emit conversasModificada();
}
void MensageiroMain::on_actionNovo_triggered()
{
    loginDialog->setUsuario(true);
}

void MensageiroMain::on_actionConectar_triggered()
{
    loginDialog->setUsuario(false);
}

void MensageiroMain::on_actionDesconectar_triggered()
{
    // Envia comando para desconectar
    write_int(s, CMD_LOGOUT_USER);

    DM.clearMeuUsuario();
    emit conversasModificada();
    emit mensagensModificada();
    emit statusModificada();
}

void MensageiroMain::on_actionSair_triggered()
{
    QCoreApplication::quit();
}

void MensageiroMain::on_actionNova_conversa_triggered()
{
    conversaDialog->open();
}

void MensageiroMain::on_tableViewConversas_activated(const QModelIndex &index)
{
    on_tableViewConversas_clicked(index);
}

void MensageiroMain::on_tableViewConversas_clicked(const QModelIndex &index)
{
    if (index.row() >= (int)DM.size())
    {
        cerr << "ID de conversa invalida.\n";
        return;
    }

    if (index.row() != DM.getIdConversa())
    {
        DM.setIdConversa(index.row());

        // Muda o status de todas as msgs que foram enviadas para mim de RECEBIDA -> LIDA
        bool houve_leitura = false;
        for (unsigned i=0; i<DM[DM.getIdConversa()].size(); i++)
        {
            if (DM[DM.getIdConversa()][i].getUsuario() == DM[DM.getIdConversa()].getUsuario() &&
                DM[DM.getIdConversa()][i].getStatus() == MSG_ENTREGUE)
            {
                DM[DM.getIdConversa()][i].setStatus(MSG_LIDA);
                // Envia msg ao servidor informando que a msg foi lida

                write_int(s, CMD_MSG_LIDA2);
                write_int(s, DM[DM.getIdConversa()][i].getId());
                write_string(s, DM[DM.getIdConversa()][i].getUsuario());

                houve_leitura = true;
            }
        }
        if (houve_leitura)
        {
            emit numMsgConversaModificado(DM.getIdConversa());
        }

        // Sinaliza para atualizar a janela de mensagens e a barra de status
        emit mensagensModificada();
        emit statusModificada();
    }
}

void MensageiroMain::on_lineEditMensagem_returnPressed()
{
    if (DM.getMeuUsuario().size() < TAM_MIN_NOME_USUARIO)
    {
        QMessageBox::warning(this, "Usuário inválido", "Usuário não definido.");
        return;
    }
    if (DM.getIdConversa()<0 || DM.getIdConversa()>=(int)DM.size())
    {
        QMessageBox::warning(this, "Conversa inválida", "Conversa não selecionada.");
        return;
    }
    string arg1 = ui->lineEditMensagem->text().toStdString();
    if (arg1.size()==0 || arg1.size()>TAM_MAX_MSG)
    {
        QMessageBox::warning(this, "Msg inválida", "Tamanho da mensagem inválido.");
        return;
    }
    ui->lineEditMensagem->clear();

    Mensagem M;
    // Define os campos da msg
    M.setId(DM.getNovaIdMensagem());
    // Nome do destinatario
    M.setUsuario(DM[DM.getIdConversa()].getUsuario());
    // Texto da mensagem
    M.setTexto(arg1);
    // Estado (status) da mensagem
    M.setStatus(MSG_ENVIADA);

    // Envia a msg M via socket para o servidor
    write_int(s, CMD_NOVA_MSG);
    write_int(s, M.getId());
    write_string(s, M.getUsuario());
    write_string(s, M.getTexto());

    // Inclui a msg na base de dados local

    // Inicialmente, troca o campo usuario da msg
    // Nas msgs de minha autoria, na base de dados local, o campo usuario eh preechido
    // com o nome do remetente (eu), para diferenciar das msgs na conversa que vieram do correspondente
    M.setUsuario(DM.getMeuUsuario());
    // Acrescenta no final da conversa
    DM[DM.getIdConversa()].insertMessage(M);
    // Sinaliza que houve alteracao no numero de msgs de uma conversa na janela de conversas
    emit numMsgConversaModificado(DM.getIdConversa());
    // Sinaliza que houve alteracao na janela de Mensagens
    emit mensagensModificada();
}
