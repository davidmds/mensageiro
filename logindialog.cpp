#include "logindialog.h"
#include "ui_logindialog.h"
#include "dadosmensageiro.h"
#include <iostream>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog),
    novoUsuario(false)
{
    ui->setupUi(this);

    ui->lineEditSenhaUsuario->setEchoMode( QLineEdit::Password );
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::setUsuario(bool NovoUsuario)
{
    novoUsuario = NovoUsuario;
    setWindowTitle(novoUsuario ? "Usuário - Criar" :
                                 "Usuário - Conectar");
    ui->lineEditNomeUsuario->clear();
    ui->lineEditSenhaUsuario->clear();
    show();
}

void LoginDialog::on_buttonBox_accepted()
{
    string login = ui->lineEditNomeUsuario->text().toStdString();
    string senha = ui->lineEditSenhaUsuario->text().toStdString();

    emit aceitaUsuario( login, senha, novoUsuario );
}
