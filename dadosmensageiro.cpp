#include <iostream>     /* cerr */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <sstream>      /* stringstream */
#include "dadosmensageiro.h"

void Mensagem::setId(uint32_t I)
{
    if (I<=0)
    {
        cerr << "Id de mensagem invalida\n";
        id = 1;
        return;
    }
    id=I;
}

void Mensagem::setUsuario(const string &U)
{
    if (U.size()<TAM_MIN_NOME_USUARIO || U.size()>TAM_MAX_NOME_USUARIO)
    {
        cerr << "Nome de usuario de mensagem invalido: " << U << endl;
        return;
    }
    usuario=U;
}

void Mensagem::setTexto(const string &T)
{
    if (T.size()==0)
    {
        cerr << "Texto de mensagem vazio\n";
        return;
    }
    texto=T;
}

void Conversa::setUsuario(const string &U)
{
    if (U.size()<TAM_MIN_NOME_USUARIO || U.size()>TAM_MAX_NOME_USUARIO)
    {
        cerr << "Nome de usuario de conversa invalido: " << U << endl;
        return;
    }
    usuario=U;
}

Mensagem &Conversa::operator[](unsigned i)
{
    static Mensagem vazia;
    return (i<size() ? LM[i] : vazia);
}

unsigned Conversa::getNumMsgsEntregues() const
{
    unsigned numMsgsEntregues=0;
    for (unsigned i=0; i<LM.size(); i++)
    {
        if (LM[i].getUsuario()==usuario &&
            LM[i].getStatus()==MSG_ENTREGUE)
        {
            numMsgsEntregues++;
        }
    }
    return numMsgsEntregues;
}


DadosMensageiro::DadosMensageiro():
    meuUsuario(""),idConversa(-1),idMensagem(0),LC()
{
    Mensagem M;
    string msg;
    MsgStatus st;
    int i,j,k;

    srand(time(NULL));

    setMeuUsuario("Adelardo Medeiros");
    // Sorteia o numero de conversas diferentes [5-25]
    for (i=0; i<5+rand()%21; i++)
    {
        stringstream ss;
        ss << "Usuario" << i+1;
        insertConversa(ss.str());
        // Sorteia o numero de msgs por conversa [10-30]
        for (j=0; j<10+rand()%21; j++)
        {
            // Atribui uma nova ID para a msg
            M.setId(getNovaIdMensagem());
            // Sorteia se a msg eh de minha autoria ou do outro usuario da conversa
            if (rand()%2)
            {
                M.setUsuario(meuUsuario);
            }
            else
            {
                M.setUsuario(LC[i].getUsuario());
            }
            // Forma a parte inicial da msg
            if (M.getUsuario()==meuUsuario)
            {
                msg = "De " + meuUsuario + " para " + LC[i].getUsuario() + ": ";
            }
            else
            {
                msg = "De " + LC[i].getUsuario() + " para " + meuUsuario + ": ";
            }
            // Sorteia o numero de caracteres da mensagem [4-255]
            for (k=0; k<4+rand()%252;k++)
            {
                msg += 'a'+rand()%26;
            }
            M.setTexto(msg);
            // Sorteia o status
            switch(1+rand()%4)
            {
            case 1:
                st = MSG_ENVIADA;
                break;
            case 2:
                st = MSG_RECEBIDA;
                break;
            case 3:
                st = MSG_ENTREGUE;
                break;
            case 4:
                st = MSG_LIDA;
                break;
            default:
                st = MSG_INVALIDA;
                break;
            }
            M.setStatus(st);
            // Adiciona a mensagem na conversa
            LC[i].insertMessage(M);
        }
    }
}

void DadosMensageiro::clearMeuUsuario()
{
    meuUsuario = "";
    setIdConversa(-1);
    clearConversas();
}

void DadosMensageiro::setMeuUsuario(const string &MU)
{
    if (MU.size()<TAM_MIN_NOME_USUARIO || MU.size()>TAM_MAX_NOME_USUARIO)
    {
        cerr << "Nome de meu usuario invalido\n";
        return;
    }
    meuUsuario = MU;
    // Sempre que altera o usuario, apaga todas as conversas
    setIdConversa(-1);
    clearConversas();
}

void DadosMensageiro::setIdConversa(int ID)
{
    if (ID >= (int)size())
    {
        cerr << "Id de conversa invalida\n";
        return;
    }
    idConversa = ID;
}

Conversa &DadosMensageiro::operator[](unsigned i)
{
    static Conversa vazia;
    return (i<size() ? LC[i] : vazia);
}

// Cria nova conversa com o nome do usuario passado como parametro
void DadosMensageiro::insertConversa(const string &U)
{
    Conversa C;
    C.setUsuario(U);
    if (U != C.getUsuario()) return;  // Erro na atribuicao do nome da conversa
    LC.push_back(C);
}

void DadosMensageiro::eraseConversa(unsigned i)
{
    if (i >= size())
    {
        cerr << "Indice de conversa a eliminar invalido\n";
        return;
    }
    LC.erase(LC.begin()+i);
}

