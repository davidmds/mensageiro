#ifndef DADOSMENSAGEIRO_H
#define DADOSMENSAGEIRO_H

#include <string>
#include <vector>
#include <stdint.h>
#include "winsocket/winsocket.h"

using namespace std;

#define TAM_MIN_NOME_USUARIO 6
#define TAM_MAX_NOME_USUARIO 32
#define TAM_MIN_SENHA 6
#define TAM_MAX_SENHA 12
#ifndef TAM_MAX_MSG
#define TAM_MAX_MSG 255
#endif

// Os possiveis estados de uma mensagem
enum MsgStatus
{
    MSG_ENVIADA=1,
    MSG_RECEBIDA=2,
    MSG_ENTREGUE=3,
    MSG_LIDA=4,
    MSG_INVALIDA=-1
};

// Os campos que compoem uma mensagem trocada entre cliente e servidor
class Mensagem
{
private:
    // Identificador da mensagem
    uint32_t id;
    // Nome do remetente ou do destinatario, dependendo do contexto
    string usuario;
    // Texto da mensagem
    string texto;
    // Estado (status) da mensagem
    MsgStatus status;
public:
    inline Mensagem(): id(0), usuario(""), texto(""), status(MSG_INVALIDA) {}
    inline uint32_t getId() const {return id;}
    void setId(uint32_t I);
    inline const string &getUsuario() const {return usuario;}
    void setUsuario(const string &U);
    inline const string &getTexto() const {return texto;}
    void setTexto(const string &T);
    inline MsgStatus getStatus() const {return status;}
    inline void setStatus(MsgStatus S) {status=S;}
};

class Conversa
{
private:
    // Correspondente (outro participante da conversa)
    string usuario;
    // Todas as mensagens da conversa
    vector<Mensagem> LM;
public:
    inline Conversa(): usuario(""), LM() {}
    inline unsigned size() const {return LM.size();}
    inline const string &getUsuario() const {return usuario;}
    void setUsuario(const string &U);
    Mensagem &operator[](unsigned i);
    inline void clearMessages() {LM.clear();}
    inline void insertMessage(const Mensagem &M) {LM.push_back(M);}
    unsigned getNumMsgsEntregues() const;
};

class DadosMensageiro
{
private:
    // O nome do usuario do cliente
    string meuUsuario;
    // O indice da conversa atualmente sendo visualizada
    int idConversa;
    // A ultima ID utilizada em msg enviada por mim
    uint32_t idMensagem;
    // Todas as mensagens de todas as conversas
    vector<Conversa> LC;
public:
    DadosMensageiro();
    inline const string &getMeuUsuario() const {return meuUsuario;}
    // Limpa o nome do usuario (Deslogar)
    void clearMeuUsuario();
    void setMeuUsuario(const string &MU);
    inline int getIdConversa() const {return idConversa;}
    void setIdConversa(int ID);
    inline int getNovaIdMensagem() {idMensagem++; return idMensagem;}
    inline unsigned size() const {return LC.size();}
    Conversa &operator[](unsigned i);
    void insertConversa(const string &U);
    void eraseConversa(unsigned i);
    inline void clearConversas() {LC.clear();}
};

// Os dados das conversas serao armazenados em uma variavel global (DM)
extern DadosMensageiro DM;
extern tcp_winsocket s;
extern HANDLE tHandle;
extern bool fim;

#endif // DADOSMENSAGEIRO_H
