#include <QApplication>

#include "config.h"
#include "mensageiro/minimess.h"
#include "mensageiromain.h"
#include "dadosmensageiro.h"
#include "socketmensageiro.h"


// A variavel global que contem todas as msgs de todas as conversas
DadosMensageiro DM;

// O socket de comunicacao com o servidor
tcp_winsocket s;

// O identificador da thread de leitura de comandos
HANDLE tHandle;

bool fim;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MensageiroMain w;
    WSADATA wsaData;
    WINSOCKET_STATUS iResult;

    // All processes that call Winsock functions must first initialize the use of the Windows Sockets DLL (WSAStartup)
    // before making other Winsock functions calls
    // The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
      cerr << "WSAStartup failed: " << iResult << endl;
      exit(1);
    }

    w.show();
    int result = a.exec();

    fim = true;

    // Desliga o socket
    s.shutdown();

    // Espera pelo fim da thread de leitura de comandos (máximo de 5 segundos)
    WaitForSingleObject(tHandle, 5000);
    // Encerra na "força bruta" a thread de comandos caso ela não tenha terminado sozinha
    // (ou seja, a função WaitForSingleObject tenha saído por timeout)
    TerminateThread(tHandle,0);
    // Encerra o handle da thread
    CloseHandle(tHandle);
    // Enderra o socket
    s.close();

    /* call WSACleanup when done using the Winsock dll */
    WSACleanup();

    return result;
}
