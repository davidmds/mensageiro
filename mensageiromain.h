#ifndef MENSAGEIROMAIN_H
#define MENSAGEIROMAIN_H

#include <QMainWindow>
#include <QLabel>
#include "modelconversas.h"
#include "modelmensagens.h"
#include "logindialog.h"
#include "conversadialog.h"

namespace Ui {
class MensageiroMain;
}

class MensageiroMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit MensageiroMain(QWidget *parent = 0);
    ~MensageiroMain();

private slots:
    void slotAtualizaBarraStatus();
    void slotAceitaUsuario(const string &login, const string &senha, bool novoUsuario );
    void slotNovaConversa(const string &usuario);
    void on_actionNovo_triggered();
    void on_actionConectar_triggered();
    void on_actionDesconectar_triggered();
    void on_actionSair_triggered();
    void on_actionNova_conversa_triggered();
    void on_tableViewConversas_activated(const QModelIndex &index);
    void on_tableViewConversas_clicked(const QModelIndex &index);
    void on_lineEditMensagem_returnPressed();

signals:
    void conversasModificada();
    void numMsgConversaModificado(unsigned I);
    void mensagensModificada();
    void statusModificada();

private:
    Ui::MensageiroMain *ui;

    ModelConversas *modelConversas;
    ModelMensagens *modelMensagens;

    QLabel *nomeUsuario;
    QLabel *nomeConversa;

    LoginDialog *loginDialog;
    ConversaDialog *conversaDialog;
};

#endif // MENSAGEIROMAIN_H
