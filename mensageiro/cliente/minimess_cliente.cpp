#include <iostream>
#include <string.h>

#include "../../winsocket/winsocket.h"
#include "../minimess.h"

using namespace std;

/* ==================================================================

Este programa tem uma thread principal que espera que o usu�rio
digite alguma coisa e envia esta msg para um servidor via
socket. Uma outra thread imprime todas as msgs recebidas pelo
socket.

================================================================== */

// Status message
typedef enum {
  MSG_ENVIADA=0,
  MSG_RECEBIDA=1,
  MSG_ENTREGUE=2,
  MSG_LIDA=3
} MSG_STATUS;

// Lista de comandos
typedef enum {
  CMD_NEW_USER=1001,
  CMD_LOGIN_USER=1002,
  CMD_LOGIN_OK=1003,
  CMD_LOGIN_INVALIDO=1004,
  CMD_NOVA_MSG=1005,
  CMD_MSG_RECEBIDA=1006,
  CMD_MSG_ENTREGUE=1007,
  CMD_MSG_LIDA1=1008,
  CMD_MSG_LIDA2=1009,
  CMD_ID_INVALIDA=1010,
  CMD_USER_INVALIDO=1011,
  CMD_MSG_INVALIDA=1012,
  CMD_LOGOUT_USER=1013
} CMD_CODES;

// Aqui vao as variaveis globais das duas threads

tcp_winsocket s;
bool fim = false;

// Esta eh a thread que escreve em tela as mensagens recebidas

DWORD WINAPI le_msg(LPVOID lpParameter){
  int32_t tempInt, id;
  string usuario, texto;

  while (!fim) {
    // L� o comando
    if(!read_int(s, tempInt)) fim = true;
    if (!fim){
      switch(tempInt) {
        case CMD_NEW_USER:
          // Ignorar. Erro do servidor
          break;
        case CMD_LOGIN_USER:
          // Ignorar. Erro do servidor
          break;
        case CMD_LOGIN_OK:
          // Ignorar, pois n�o veio ap�s um pedido de cria��o de usu�rio ou conex�o de usu�rio
          break;
        case CMD_LOGIN_INVALIDO:
          // Ignorar, pois n�o veio ap�s um pedido de cria��o de usu�rio ou conex�o de usu�rio
          break;
        case CMD_NOVA_MSG:
          //TODO: se n�o existe conversa com o usu�rio remetente: criar conversa
          // Insere nova mensagem na conversa com o status MSG_ENTREGUE
          // Caso a conversa esteja sendo visualizada: Altera o status para MSG_LIDA e envia CMD_MSG_LIDA1

          if(!read_int(s, id)){
            fim = true;
            break;
          }
          if(!read_string(s, usuario)){
            fim = true;
            break;
          }
          if(!read_string(s, texto)){
            fim = true;
            break;
          }

          cout << endl
               << "=========================================================\n"
               << "Mensagem recebida de " << usuario <<":\n"
               << texto << endl
               << "=========================================================\n";
          break;
        case CMD_MSG_RECEBIDA:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA
          // Se ok: Status = MSG_RECEBIDA e altera (visualmente) o status
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_MSG_ENTREGUE:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA ou MSG_RECEBIDA
          // Se ok: Status = MSG_ENTREGUE e altera (visualmente) o status
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_MSG_LIDA1:
          // Ignorar. Erro do servidor
          break;
        case CMD_MSG_LIDA2:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA, MSG_RECEBIDA ou MSG_ENTREGUE
          // Se ok: Status = MSG_LIDA e altera (visualmente) o status
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_ID_INVALIDA:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA
          // Se ok: Remove a mensagem do buffer e exibe mensagem de erro
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_USER_INVALIDO:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA
          // Se ok: Remove a mensagem do buffer e exibe mensagem de erro
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_MSG_INVALIDA:
          //TODO: Testa se mensagem.id existe, mensagem.remetente sou eu e mensagem.status � MSG_EMVIADA
          // Se ok: Remove a mensagem do buffer e exibe mensagem de erro
          // Se n�o: Ignorar. Erro do servidor ou do cliente
          break;
        case CMD_LOGOUT_USER:
          // Ignorar. Erro do servidor
          break;
        default:
          // Ignorar. Erro do servidor
          break;
      }
    }
  }
  return 0;
}

// O programa principal cont�m o ciclo que envia as mensagens digitadas

int main(int argc, char **argv){
  WSADATA wsaData;
  HANDLE tHandle;
  int32_t tempInt;
  string usuario, senha;
  string texto;
  WINSOCKET_STATUS iResult;

  // All processes that call Winsock functions must first initialize the use of the Windows Sockets DLL (WSAStartup)
  // before making other Winsock functions calls
  // The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
  iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
  if (iResult != 0) {
    cerr << "WSAStartup failed: " << iResult << endl;
    exit(1);
  }

  // Determina o numero IP do servidor (digitado ou lido do argumento de chamada)
  if( argc<2 ){
    cout << "Maquina onde estah rodando o servidor (IP): ";
    cin >> ws;
    getline(cin, texto);
  } else {
    texto = argv[1];
  }

  // Conecta com o servidor
  if (s.connect(texto.c_str(), PORTA_TESTE) == SOCKET_OK){
    cout << "Conectado ao servidor " << texto << " na porta " << PORTA_TESTE << endl;
  } else {
    cerr << "Problema na conexao ao servidor " << texto << " na porta " << PORTA_TESTE << endl;
    exit(1);
  }

  // Cria a thread que escreve as mensagens recebidas
  tHandle = CreateThread(NULL, 0, le_msg, NULL , 0, NULL);
  if (tHandle == NULL){
    cerr << "Problema na criacao da thread: " << GetLastError() << endl;
    exit(1);
  }

  // Envia para o servidor o nome de usuario que identificarah esse cliente
  do{
    if(tempInt == CMD_LOGIN_INVALIDO) cout << "Usuario ou senha errado. Tente novamente.\n";

    if(!write_int(s,CMD_LOGIN_USER)) exit(1);
    do{
      cout << "Login para esse cliente("<< usuario <<"): ";
      cin >> ws;
      getline(cin, usuario);
    } while (usuario.size() < 6 || usuario.size() > 32);
    if(!write_string(s,usuario)) exit(1);

    do{
      cout << "Senha para esse cliente: ";
      cin >> ws;
      getline(cin, senha);
    } while (senha.size() < 6 || senha.size() > 12);
    if(!write_string(s,senha)) exit(1);
  } while(read_int(s,tempInt) && tempInt != CMD_LOGIN_OK);

  // Este � o ciclo que envia as mensagens digitadas
  while (!fim){
    do{
      cout << "Usuario a enviar msg [ALL para todos, FIMFIM para terminar]: ";
      cin >> ws;
      getline(cin, usuario);
    } while (usuario.size() < 6 || usuario.size() > 32);
    fim = (usuario=="FIMFIM");
    if(!fim)
      do{
        cout << "Mensagem a enviar [max " << TAM_MAX_MSG << " caracteres, FIMFIM para terminar]: ";
        cin >> ws;
        getline(cin, texto);
      } while (texto.size()==0 || texto.size()>TAM_MAX_MSG);
    fim = (texto=="FIMFIM");

    if(!fim) if(!write_int(s,CMD_NOVA_MSG)) fim = true;
    tempInt = 1;
    if(!fim) if(!write_int(s,tempInt)) fim = true;
    if(!fim) if(!write_string(s,usuario)) fim = true;
    if(!fim) if(!write_string(s,texto)) fim = true;
    Sleep(1000); // Espera 1 seg para as mensagens n�o se sobreporem no terminal
  }

  // Desliga o socket
  s.shutdown();
  // Espera pelo fim da thread de recep��o (m�ximo de 5 segundos)
  cout << "Aguardando o encerramento da outra thread...\n";
  WaitForSingleObject(tHandle, 5000);
  // Encerra na for�a bruta a thread de recepcao caso ela nao tenha terminado sozinha
  // (ou seja, a funcao WaitForSingleObject tenha sa�do por timeout)
  TerminateThread(tHandle,0);
  // Encerra o handle da thread
  CloseHandle(tHandle);
  // Encerra o socket
  s.close();

  /* call WSACleanup when done using the Winsock dll */
  WSACleanup();
}
