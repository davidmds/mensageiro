#ifndef MINIMESS_H
#define MINIMESS_H

#include <string.h>
#include "../winsocket/winsocket.h"

using namespace std;

#define PORTA_TESTE "23456"
#ifndef TAM_MAX_MSG
#define TAM_MAX_MSG 256
#endif
#define NUM_MAX_CONEX 30

bool read_int(tcp_winsocket &s, int32_t &num);
bool write_int(tcp_winsocket &s, const int32_t num);
bool read_string(tcp_winsocket &s, string &msg);
bool write_string(tcp_winsocket &s, const string &msg);

#endif // MINIMESS_H
