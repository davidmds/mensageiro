#include <iostream>
#include <string.h>
#include <list>
#include <algorithm>

#include "../../winsocket/winsocket.h"
#include "../minimess.h"

using namespace std;

// Status message
typedef enum {
  MSG_ENVIADA=0,
  MSG_RECEBIDA=1,
  MSG_ENTREGUE=2,
  MSG_LIDA=3
} MSG_STATUS;

// Lista de comandos
typedef enum {
  CMD_NEW_USER=1001,
  CMD_LOGIN_USER=1002,
  CMD_LOGIN_OK=1003,
  CMD_LOGIN_INVALIDO=1004,
  CMD_NOVA_MSG=1005,
  CMD_MSG_RECEBIDA=1006,
  CMD_MSG_ENTREGUE=1007,
  CMD_MSG_LIDA1=1008,
  CMD_MSG_LIDA2=1009,
  CMD_ID_INVALIDA=1010,
  CMD_USER_INVALIDO=1011,
  CMD_MSG_INVALIDA=1012,
  CMD_LOGOUT_USER=1013
} CMD_CODES;

// Classe que armazena os dados que definem uma mensagem
struct Message{
  int32_t id;
  string remetente, texto;
  MSG_STATUS status;
  inline Message(): id(), remetente(""), texto(""), status() {}
};

// Classe que armazena os dados que definem um cliente
struct Client{
  string login, senha;
  tcp_winsocket socket;
  list<Message> waitingMessages;
  inline Client(): login(""), senha(""), socket(), waitingMessages() {}
  inline bool operator==(const string &L) {return login==L;}
  inline bool operator==(const Client &L) {return login==L.login;}
};

// Apelidos para uma lista de clientes e para o iterator correspondente
typedef list<Client> list_Client;
typedef list_Client::iterator iter_Client;


#define TEMPO_MAXIMO 60000  // Tempo m�ximo de espera em milisegundos
#define TEMPO_ENVIO_LOGIN 60 // Tempo m�xido de espera de envio do login ap�s conectar

//
// Vari�veis globais das 2 threads
//
// O socket de conexoes
tcp_winsocket_server socket_conexoes;
// A lista de clientes
list_Client LC;

// O flag que indica que o software deve encerrar todas as threads
bool fim = false;

bool validaTexto(const string &texto){
    return !(texto.length() < 0 || texto.length() > 255);
}
bool validaUsuario(const string &usuario){
    return !(usuario.length() < 6 || usuario.length() > 32);
}
bool validaSenha(const string &senha){
    return !(senha.length() < 6 || senha.length() > 12);
}
bool validaUsuarioSenha(const string &usuario, const string &senha){
  return validaUsuario(usuario) && validaSenha(senha);
}
bool lerUsuarioSenha(tcp_winsocket &socket, string &usuario, string &senha){
  if(!read_string(socket,usuario)) return false;
  if(!read_string(socket,senha)) return false;
  return true;
}

bool enviarMsg(tcp_winsocket &socket, const Message &msg){
    if(!write_int(socket, CMD_NOVA_MSG)) return false;
    if(!write_int(socket, msg.id)) return false;
    if(!write_string(socket, msg.remetente)) return false;
    if(!write_string(socket, msg.texto)) return false;

    return true;
}

// Thread que efetivamente desempenha as tarefas do servidor
DWORD WINAPI servidor(LPVOID lpParameter){
  tcp_winsocket t_socket;
  winsocket_queue fila_sockets;
  WINSOCKET_STATUS iResult;

  int32_t tempInt;
  string usuario, senha, msg;

  iter_Client itClient, itClient2;

  Message NovaMsg;
  bool validar;


  while (!fim){
    // Inclui na fila de sockets para o select todos os sockets que eu
    // quero monitorar para ver se houve chegada de dados
    fila_sockets.clean();
    if (!(fim = !socket_conexoes.accepting())){
      fila_sockets.include(socket_conexoes);
      for (iter_Client it = LC.begin(); it != LC.end(); ++it)
    	if (it->socket.connected())
	      fila_sockets.include(it->socket);
    }

    // Espera que chegue alguma dados em qualquer dos sockets da fila
    iResult = fila_sockets.wait_read(TEMPO_MAXIMO);
    if (iResult==SOCKET_ERROR){
      if (!fim) cerr << "Erro na espera por alguma atividade\n";
      fim = true;
    }
    if (!fim){
      if (iResult!=0){
        // N�o saiu por timeout: houve atividade em algum socket da fila
        // Testa em qual socket houve atividade.

        // Primeiro, testa os sockets dos clientes
        for (iter_Client it = LC.begin(); it != LC.end(); ++it){
          if (it->socket.connected() && fila_sockets.had_activity(it->socket)){
            // L� o comando
            if(!read_int(t_socket, tempInt)) continue;

            switch(tempInt) {
              case CMD_NEW_USER:
                //Usu�rio j� conectado
                cout << "Usuario ja conectado. Nao pode CMD_NEW_USER. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_LOGIN_USER:
                //Usu�rio j� conectado
                cout << "Usuario ja conectado. Nao pode CMD_LOGIN_USER. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_LOGIN_OK:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_LOGIN_INVALIDO:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_NOVA_MSG:
                if(!read_int(it->socket, tempInt)) break; // id da mensagem
                if(!read_string(it->socket, usuario)) break;
                if(!read_string(it->socket, msg)) break;

                if(!validaUsuario(usuario)){
                    if(!write_int(it->socket, CMD_USER_INVALIDO)) break;
                    it->socket.shutdown();
                    break;
                }
                // tamanho usuario valido
                // Verifica se usu�rio existe
                itClient = find(LC.begin(), LC.end(), usuario);
                if(itClient == LC.end()){
                    if(!write_int(it->socket, CMD_USER_INVALIDO)) break;
                    it->socket.shutdown();
                    break;
                }
                // Usu�rio existe
                // Validar id
                validar = true;
                for (iter_Client i = LC.begin(); i != LC.end() && validar; i++){
                    if(i->waitingMessages.size() > 0){
                        for (list<Message>::iterator j = i->waitingMessages.begin(); j != i->waitingMessages.end() && validar; j++){
                            if(j->remetente == it->login && j->id == tempInt){
                                // J� existe uma mensagem com o mesmo id vinda deste cliente
                                validar = false;
                                break; // saindo do for
                            }
                        }
                    }
                }
                if(!validar){
                    if(!write_int(it->socket, CMD_ID_INVALIDA)) break;
                    it->socket.shutdown();
                    break;
                }
                // id � valido
                if(!validaTexto(msg)){
                    if(!write_int(it->socket, CMD_MSG_INVALIDA)) break;
                    it->socket.shutdown();
                    break;
                }

                // Tudo v�lido
                NovaMsg.id = tempInt;
                NovaMsg.remetente = it->login;
                NovaMsg.texto = msg;
                NovaMsg.status = MSG_RECEBIDA;

                if(itClient->socket.connected()){
                    // Cliente destinat�rio est� online. Enviar mensagem
                    if(!enviarMsg(itClient->socket, NovaMsg)) break;
                    if(!write_int(it->socket, CMD_MSG_ENTREGUE)) break;
                    NovaMsg.status = MSG_ENTREGUE;
                }
                itClient->waitingMessages.push_back(NovaMsg);

                cout << "De:" << it->login << "; Para: " << usuario << "; Mensagem: " << msg << endl;
                break;
              case CMD_MSG_RECEBIDA:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_MSG_ENTREGUE:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_MSG_LIDA1:
                if(!read_int(it->socket, tempInt)) break; // id da mensagem
                if(!read_string(it->socket, usuario)) break;

                validar = false;
                if(it->waitingMessages.size() > 0){
                    for(list<Message>::iterator j = it->waitingMessages.begin(); j != it->waitingMessages.end(); j++){
                        if(j->remetente == usuario && j->id == tempInt){
                            // Existe uma mensagem com o mesmo id e vinda deste cliente

                            // Avisa que encontrou a mensagem para o if a baixo
                            validar = true;

                            // Procura o cliente remetente e envia CMD_MSG_LIDA2 para ele
                            // Altera status da mensagem no buffer para MSG_LIDA
                            itClient = find(LC.begin(), LC.end(), usuario);
                            if(itClient != LC.end()){
                                if(!write_int(itClient->socket, CMD_MSG_LIDA2)) break;
                                j->status = MSG_LIDA;
                            }
                            break; // saindo do for
                        }
                    }
                    // N�o encontrou a mensagem
                    if(!validar) it->socket.shutdown();
                } else {
                    // N�o existe mensagens no buffer
                    it->socket.shutdown();
                    break;
                }
                break;
              case CMD_MSG_LIDA2:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_ID_INVALIDA:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_USER_INVALIDO:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_MSG_INVALIDA:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
              case CMD_LOGOUT_USER:
                //Deslogando cliente
                cout << "O usuario " << it->login << " desconectou.\n";
                it->socket.shutdown();
                break;
              default:
                //Erro do cliente
                cout << "Codigo errado vindo do cliente. Desconentando\n";
                it->socket.shutdown();
                break;
            }
          }
        }

        // Depois, testa se houve atividade no socket de conexao
        if (fila_sockets.had_activity(socket_conexoes)){
          if (socket_conexoes.accept(t_socket) != SOCKET_OK){
            cerr << "N�o foi poss�vel estabelecer uma conexao\n";
            fim = true;
          }
          if (!fim){
            // L� o comando
            read_int(t_socket, tempInt);
            switch (tempInt) {
              case CMD_NEW_USER:
                // Ler usuario e senha
                if(!lerUsuarioSenha(t_socket, usuario, senha)){
                  t_socket.close();
                  break;
                }
                // Valida o tamanho do usuario e da senha
                if(!validaUsuarioSenha(usuario, senha)){
                  cout << "Usuario ou senha com tamanho errado. Usuario: " << usuario << "; Senha: " << senha << endl;
                  if(!write_int(t_socket, CMD_LOGIN_INVALIDO)) break;
                  t_socket.close();
                  break;
                }
                // Procura se j� tem um usu�rio com o nome inserido
                itClient = find(LC.begin(), LC.end(), usuario);
                if(itClient != LC.end()){
                  // O usu�rio j� existe
                  cout << "O usuario " << usuario << " ja existe\n";
                  if(!write_int(t_socket, CMD_LOGIN_INVALIDO)) break;
                  t_socket.close();
                } else {
                  // Usu�rio n�o existe
                  if(!write_int(t_socket, CMD_LOGIN_OK)) break;
                  Client novo;
                  novo.login = usuario;
                  novo.senha = senha;
                  novo.socket = t_socket;
                  LC.push_back(novo);
                  cout << "Cliente " << usuario << " cadastrado e conectado.\n";
                }
                break;
              case CMD_LOGIN_USER:
                // Ler usuario e senha
                if(!lerUsuarioSenha(t_socket, usuario, senha)){
                  t_socket.close();
                  break;
                }
                // Valida o tamanho do usuario e da senha
                if(!validaUsuarioSenha(usuario, senha)){
                  cout << "Usuario ou senha com tamanho errado.\n";
                  if(!write_int(t_socket, CMD_LOGIN_INVALIDO)) break;
                  t_socket.close();
                  break;
                }
                // Verifica se o usuario existe e a senha esta certa
                itClient = find(LC.begin(), LC.end(), usuario);
                if(itClient != LC.end()){
                  // Usu�rio encontrado
                  if(itClient->senha == senha){
                    // Senha correta
                    if(!write_int(t_socket, CMD_LOGIN_OK)){
                      t_socket.close();
                      break;
                    }
                    cout << "Usuario " << usuario << " conectou.\n";
                    itClient->socket = t_socket;

                    //TODO: Enviar mensagens pendentes que est�o em waitingMessages
                    if(itClient->waitingMessages.size() > 0){
                        for(list<Message>::iterator j = itClient->waitingMessages.begin(); j != itClient->waitingMessages.end(); j++){
                            if(j->status == MSG_RECEBIDA){
                                if(!enviarMsg(itClient->socket, *j)) break;
                                j->status = MSG_ENTREGUE;

                                itClient2 = find(LC.begin(), LC.end(), j->remetente);
                                if(itClient2 != LC.end()){
                                    if(itClient2->socket.connected()){
                                        write_int(itClient2->socket, CMD_MSG_ENTREGUE);
                                        write_int(itClient2->socket, j->id);
                                    }
                                }
                            }

                        }
                    }
                  } else {
                    // senha errada
                    cout << "Senha errada. Usuario: " << usuario << endl;
                    write_int(t_socket, CMD_LOGIN_INVALIDO);
                    t_socket.close();
                    break;
                  }
                } else {
                  // n�o encontrou o usu�rio
                  cout << "Usuario errado.\n";
                  write_int(t_socket, CMD_LOGIN_INVALIDO);
                  t_socket.close();
                  break;
                }
                break;
              default:
                cout << "Apenas CMD_NEW_USER ou CMD_LOGIN_USER eh aceito como primeiro comando. Desconectando\n";
                t_socket.shutdown();
            }
          }
        }
      } else {
        // Saiu por timeout: n�o houve atividade em nenhum socket da fila
        if (LC.empty()){
          cout << "Servidor inativo hah " << TEMPO_MAXIMO << " segundos...\n";
        }
      }

      // Depois de testar a chegada de dados em todos os sockets,
      // elimina da lista de sockets as conexoes que foram fechadas porque houve
      // falha na comunicacao ou porque se desligaram
      for (iter_Client i = LC.begin(); i != LC.end(); i++){
        if (!i->socket.connected()){
          i->socket.close();
          i = LC.erase(i);
        }
      }
    }
  }
  return 0;
}

int main(void)
{
  WSADATA wsaData;
  string msg;

  // All processes that call Winsock functions must first initialize the use of the Windows Sockets DLL (WSAStartup)
  // before making other Winsock functions calls
  // The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
  WINSOCKET_STATUS iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
  if (iResult != 0) {
    cerr << "WSAStartup failed: " << iResult << endl;
    exit(1);
  }

  if (socket_conexoes.listen(PORTA_TESTE,NUM_MAX_CONEX) != SOCKET_OK) {
    cerr << "N�o foi poss�vel abrir o socket de controle\n";
    exit(1);
  }

  // Cria a thread que recebe e reenvia as mensagens
  HANDLE tHandle = CreateThread(NULL, 0, servidor, NULL , 0, NULL);
  if (tHandle == NULL){
    cerr << "Problema na cria��o da thread: " << GetLastError() << endl;
    exit(1);
  }

  while (!fim){
    cout << "Digite 'fim' para sair. \n";
    cin >> ws;
    getline(cin,msg);
    fim = (msg=="fim");
  }

  // Desliga os sockets
  cout << "Encerrando o socket de conexoes\n";
  socket_conexoes.shutdown();
  for (iter_Client it = LC.begin(); it != LC.end(); ++it){
    cout << "Encerrando o socket do cliente " << it->login << endl;
    it->socket.shutdown();
  }
  // Espera pelo fim da thread do servidor (m�ximo de 5 segundos)
  cout << "Aguardando o encerramento da outra thread...\n";
  WaitForSingleObject(tHandle, 5000);
  // Encerra na "for�a bruta" a thread do servidor caso ela n�o tenha terminado sozinha
  // (ou seja, a fun��o WaitForSingleObject tenha sa�do por timeout)
  TerminateThread(tHandle,0);
  // Encerra o handle da thread
  CloseHandle(tHandle);
  // Encerra o socket
  socket_conexoes.close();
  for (iter_Client it = LC.begin(); it != LC.end(); ++it) it->socket.close();

  /* call WSACleanup when done using the Winsock dll */
  WSACleanup();
}
