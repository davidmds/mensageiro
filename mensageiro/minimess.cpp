#include <iostream>
#include <string.h>

#include "../winsocket/winsocket.h"
#include "minimess.h"

using namespace std;

// Rotinas para ler e escrever um inteiro em um socket
// Para toda mensagem recebida ou enviada,
// s�o transmitidos os "n" bytes que representam o numero
// Retorna o numero de bytes que foram lidos ou SOCKET_ERROR em caso de erro
bool read_int(tcp_winsocket &s, int32_t &num){
  if(s.connected()){
    WINSOCKET_STATUS iResult;

    iResult = s.read((char*)&num,sizeof(num)); // sizeof(num) == 4
    if ( iResult != sizeof(num) ){
      cerr << "Nao foi poss�vel ler o inteiro no socket " << s << ". Desconectando" << endl;
      s.shutdown();
      return false;
    }
    return true;
  }
  return false;
}

// Retorna o numero de bytes que foram enviados ou SOCKET_ERROR em caso de erro
bool write_int(tcp_winsocket &s, const int32_t num){
  if(s.connected()){
    WINSOCKET_STATUS iResult;

    iResult = s.write((char*)&num,sizeof(num)); // sizeof(num) == 4
    if ( iResult != sizeof(num) ){
      cerr << "Nao foi poss�vel escrever o inteiro no socket " << s << ". Desconectando" << endl;
      s.shutdown();
      return false;
    }
    return true;
  }
  return false;
}

// Rotinas para ler e escrever uma string em um socket
// Para toda mensagem recebida ou enviada, inicialmente �
// transmitido o n�mero "n" de bytes da mensagem
// e em seguida os "n" bytes da mensagem.
// Retorna o numero de bytes que foram lidos ou SOCKET_ERROR em caso de erro
bool read_string(tcp_winsocket &s, string &msg){
  if(s.connected()){
    static char buffer[TAM_MAX_MSG+1];
    int32_t len;
    WINSOCKET_STATUS iResult;

    // Le o numero de caracteres da string
    if (!read_int(s,len)){
      cerr << "Nao foi poss�vel ler o tamanho da msg no socket " << s << ". Desconectando" << endl;
      msg = "";
      return false;
    }
    if (len > TAM_MAX_MSG){
      cerr << "Mensagem de tamanho muito grande para ser lida: " << len << ". Desconectando" << endl;
      msg = "";
      s.shutdown();
      return false;
    }
    // Le os caracteres da string
    iResult = s.read(buffer,len);
    if ( iResult != len ){
      cerr << "Nao foi poss�vel ler a msg no socket " << s << ". Desconectando" << endl;
      msg = "";
      s.shutdown();
      return false;
    }
    // Retorna uma string C++ a partir de buffer, que eh um array de char ("string" C)
    buffer[len] = 0;
    msg = buffer;  // Faz a conversao de char* para string ao atribuir
    return true;
  }
  return false;
}

// Retorna o numero de bytes que foram enviados ou SOCKET_ERROR em caso de erro
bool write_string(tcp_winsocket &s, const string &msg){
  if(s.connected()){
    int32_t len;
    WINSOCKET_STATUS iResult;

    len = msg.size();
    if (!write_int(s,len)){
      cerr << "Nao foi poss�vel escrever o tamanho da msg no socket " << s << ". Desconectando" << endl;
      return false;
    }
    iResult = s.write(msg.c_str(),len);
    if ( iResult != len ){
      cerr << "Nao foi poss�vel escrever a msg no socket " << s << ". Desconectando" << endl;
      s.shutdown();
      return false;
    }
    return true;
  }
  return false;
}
