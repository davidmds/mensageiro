#include "conversadialog.h"
#include "ui_conversadialog.h"
#include "dadosmensageiro.h"
#include <iostream>

ConversaDialog::ConversaDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConversaDialog)
{
    ui->setupUi(this);
    setWindowTitle("Nova conversa");

}

ConversaDialog::~ConversaDialog()
{
    delete ui;
}

void ConversaDialog::open()
{
    ui->lineEdit->clear();

    show();
}

void ConversaDialog::on_buttonBox_accepted()
{
    string usuario = ui->lineEdit->text().toStdString();

    emit novaConversa( usuario );
}
